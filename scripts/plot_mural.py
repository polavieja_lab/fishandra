#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1 import ImageGrid

from fishandra.script_utils import (load_model_and_update_constants,
                                    generate_arg_parser)
from fishandra.plot.new_constants import constants as cons
from fishandra.plot.maps_utils import (remove_strange_white_lines,
                                    produce_subnetwork_output_many_speeds)

#working

ACCELERATION = 0 #5e-3


def text_left_of_ax(fig=None, ax=None, text=None, offset=0.15, fontsize=12):
    position_box = ax.get_position()
    centery = 0.5*(position_box.y0 + position_box.y1)
    return fig.text(position_box.x0 - offset, centery,
                    text, verticalalignment='center', fontsize=fontsize,
                    horizontalalignment='center', rotation=90)


def text_bottom_of_ax(fig=None, ax=None, text=None, offset=0.15, fontsize=12):
    box = ax.get_position()
    centerx = 0.5*(box.x0 + box.x1)
    return fig.text(centerx, box.y0-offset, text,
                    verticalalignment='center', fontsize=fontsize,
                    horizontalalignment='center')


def add_axes_labels(fig=None, axes=None, xlabels=[], ylabels=[],
                    xoffset=0.07, yoffset=0.1):
    for ax, xlabel in zip(axes[-1], xlabels):
        text_bottom_of_ax(fig=fig, ax=ax, text=xlabel, offset=yoffset)
    for ax_row, ylabel in zip(axes, ylabels):
        text_left_of_ax(fig=fig, ax=ax_row[0], text=ylabel, offset=xoffset)


def model_factory(attention=False):
    if attention:
        return AttentionStudiedModel
    else:
        return ProbStudiedModel

class VelocitySetter:
    v = 0.005
    vnx = 0.0
    vny = 0.005

    def __init__(self, models):
        self.models = models
        self.fig, self.ax = plt.subplots(1)
        self.ax.set_aspect('equal', adjustable='box')
        self.ax_focal = make_axes_locatable(
            self.ax).append_axes(
            "right", size="5%", pad="2%")
        # Connecting models
        for model in models:
            model.set_vel(focal_v=self.v, nb_v=[self.vnx, self.vny])
        # Nb speed plot
        self.sc = self.ax.scatter(model.vnx, model.vny)
        circle1 = plt.Circle(
            (0, 0), cons.MAX_SPEED, fc='none', ec='k', linewidth=0.1)
        circle2 = plt.Circle(
            (0, 0), cons.MEDIAN_SPEED, fc='none', ec='k', linewidth=0.1)
        self.ax.add_patch(circle1)
        self.ax.add_patch(circle2)
        self.ax.axvline(x=0, linewidth=0.1, color='k')
        self.ax.axhline(y=0, linewidth=0.1, color='k')
        self.ax.set_xlim([-cons.MAX_SPEED, cons.MAX_SPEED])
        self.ax.set_ylim([-cons.MAX_SPEED, cons.MAX_SPEED])
        # Focal speed plot
        self.sc_focal = self.ax_focal.scatter(0, model.v)
        self.ax_focal.set_ylim([0, cons.MAX_SPEED])
        self._cidpress = self.fig.canvas.mpl_connect(
            'button_press_event', self.on_press)

    def on_press(self, event):
        print("Click!")
        if event.inaxes is self.ax:
            self.on_press_neighbour(event)
        elif event.inaxes is self.ax_focal:
            self.on_press_focal(event)
        self.fig.canvas.draw()

    def on_press_neighbour(self, event):
        print("Warning, unused dial")
        pass

    def on_press_focal(self, event):
        self.change_focal_speed_to(event.ydata)
        self.coordinate_and_plot()

    def change_focal_speed_to(self, new_v):
        self.sc_focal.set_offsets([0.0, new_v])
        for model in self.models:
            print("Setting speed to {} BL/s".format(new_v*cons.DELTA_V))
            model.set_vel(focal_v=new_v)

    def coordinate_and_plot(self):
        vmin = []
        vmax = []
        for model in self.models:
            model.update()
            vmin.append(model.zmin)
            vmax.append(model.zmax)
        for model in self.models:
            model.set_limits(min(vmin), max(vmax))
            model.plot()

    def __del__(self):
        self.fig.canvas.mpl_disconnect(self._cidpress)


class ProbStudiedModel:
    model_name = 'pairwise_model'
    show_negatives = True
    v = 0.005
    num_axes = 1
    vnx = 0  # To remove
    vny = 0  # To remove
    symmetric = [True, False]
    CS = [None, None]

    def __init__(self, model, px=31, numlevels=40, polar=True, fig=None,
                 ax=None,
                 cax=None, plot_options={}, x=0, y=0):
        self.x, self.y = x/cons.DELTA_X, y/cons.DELTA_X
        self.px = px
        self.numlevels = numlevels
        self.ticks = np.arange(0, 1.0, 0.1)
        self.plot_options = plot_options
        self.symmetric = True
        self.CS = None
        self.model = model
        self.polar = polar
        self.fig = fig
        self.ax = ax
        if cax is None:
            self.cax = make_axes_locatable(self.ax).append_axes(
                "right", size="5%", pad="2%")  # for ax in self.ax]
        else:
            self.cax = cax
        self.set_grid()

    def adapt_to_plot(self, v):
        return v

    def set_grid(self):
        if self.polar:
            vx = np.linspace(-np.pi, np.pi, self.px)
            vy = np.linspace(0, cons.SPEED_ALL[95], self.px)
        else:
            vx = np.linspace(-cons.SPEED_ALL[95], cons.SPEED_ALL[95], self.px)
            vy = np.linspace(-cons.SPEED_ALL[95], cons.SPEED_ALL[95], self.px)
        self.Vx, self.Vy = np.meshgrid(vx, vy)
        #self.Z = self.predict_restricted(self.Vx, self.Vy)

    def set_vel(self, focal_v=None, nb_v=None):
        if focal_v is not None:
            self.v = focal_v
        if nb_v is not None:
            print("Warning: no change in speed")

    def predict_restricted(self, Vx, Vy):
        assert(Vx.shape == Vy.shape)
        if self.polar:
            vx = Vy*np.sin(Vx)
            vy = Vy*np.cos(Vx)
        else:
            vx = Vx
            vy = Vy

        Z = produce_subnetwork_output_many_speeds(self.model, self.model_name,
                                                  vx, vy, x=self.x, y=self.y,
                                                  afn=ACCELERATION)
        return Z

    def set_limits(self, vmin, vmax):
        self.vmin = vmin
        self.vmax = vmax

    @property
    def zmax(self):
        if self.symmetric:
            zmax = abs(self.Z).max()
        else:
            zmax = self.Z.max()
        return zmax

    @property
    def zmin(self):
        if self.symmetric:
            zmin = -abs(self.Z).max()
        else:
            zmin = self.Z.min()
        return zmin

    def limits(self):
        try:
            return self.vmin, self.vmax
        except BaseException:
            return self.zmin, self.zmax

    def update(self):
        self.Z = self.predict_restricted(self.Vx, self.Vy)

    def plot(self, leader=None):
        vmin, vmax = self.limits()
        Vx = self.Vx
        Vy = self.Vy * cons.DELTA_V

        P = self.adapt_to_plot(self.Z)
        Pmin = self.adapt_to_plot(vmin)
        Pmax = self.adapt_to_plot(vmax)
        if self.show_negatives:
            Plines = np.linspace(Pmin, Pmax, self.numlevels, endpoint=True)
        else:
            Plines = np.linspace(0, Pmax, self.numlevels, endpoint=True)

        self.CS = self.ax.contourf(Vx, Vy, P, Plines,
                                   cmap=plt.cm.seismic, vmin=Pmin, vmax=Pmax,
                                   origin='lower')
        remove_strange_white_lines(self.CS)

        cbar = self.fig.colorbar(self.CS, cax=self.cax)
        tick_locator = ticker.MaxNLocator(nbins=5, integer=True)
        cbar.locator = tick_locator
        cbar.update_ticks()

        cbar.set_label("Logit of focal fish turning right after 1 s", rotation=90)

        self.ax.axvline(x=0, linewidth=0.5, color='k', linestyle='dashed')
        self.fig.canvas.draw()


class AttentionStudiedModel(ProbStudiedModel):
    ticks = None
    show_negatives = False
    model_name = 'attention_model'

def study_model(args):
    model = load_model_and_update_constants(args, one_neighbour=True)

    print("Acceleration = ", ACCELERATION*cons.DELTA_A)
    StudiedModel = model_factory(attention=args.attention)
    print("Num neighbours: ", model.args['num_neighbours'])
    print("Future steps: ", model.args['future_steps'])

    x_a = [-7, -5, -3, -1, 1, 3, 5, 7]
    y_a = x_a[::-1]
    models = []
    fig = plt.figure(1, (7,5))
    grid = ImageGrid(fig, 111, axes_pad=(0.1, 0.08),
                     nrows_ncols = (len(x_a),len(y_a)),
                     cbar_mode = "single", aspect=False, label_mode="1")
    x_label = [
        r"-$\pi$",
        r"-$\frac{\pi}{2}$",
        r"$0$",
        r"$\frac{\pi}{2}$",
        r"$\pi$"]


    grid.axes_row[-1][0].xaxis.set_ticks(np.array([-1.0, -0.5, 0.0, 0.5, 1.0])*np.pi)
    grid.axes_row[-1][0].xaxis.set_ticklabels(x_label)
    grid.axes_row[-1][0].set_xlabel(r"$\theta_i$ (rad)", labelpad=0.03)
    grid.axes_row[-1][0].set_ylabel(r"$v_i$ (BL/s)", labelpad=0.03)


    cax = grid.cbar_axes[0]
    ax = grid.axes_row

    for i, y in enumerate(y_a):
        for j, x in enumerate(x_a):
            grid.axes_row[i][j].xaxis.set_ticks(np.array([-1.0, -0.5, 0.0, 0.5, 1.0])*np.pi)
            if y is y_a[-1] and x is x_a[0]:
                options = {
                    'xlabel': r'$\theta$ (rad)',
                    'ylabel': r'$v_i$ (BL/s)'}
            elif y is y_a[0] and x is x_a[-1]:
                options = {
                    'xlabel': r'$\theta_i$ (rad)',
                    'ylabel': r'$v_i$ (BL/s)',
                    'xlabelposition': 'top',
                    'ylabelposition': 'right'}
            else:
                options = {'remove_ticks': True}

            models.append(
                StudiedModel(
                    model,
                    cax=cax,
                    fig=fig,
                    ax=ax[i][j],
                    x=x,
                    y=y,
                    plot_options=options))

    EXTRA_FRAMES = [[7,1], [3,1]]
    if EXTRA_FRAMES:
        fig2 = plt.figure(2, (6.5,2.2))
        grid2 = ImageGrid(fig2, 111, nrows_ncols = (1,len(EXTRA_FRAMES)),
                          axes_pad=0.25, cbar_mode = None,
                          aspect=False, label_mode="L")
        for i, coords in enumerate(EXTRA_FRAMES):
            x, y = coords
            models.append(
                StudiedModel(
                    model,
                    px = 31,
                    numlevels = 100,
                    cax=grid2.cbar_axes[0],
                    fig=fig2,
                    ax=grid2[i],
                    x=x,
                    y=y,
                    plot_options=options))
            grid2[i].xaxis.set_ticks(np.array([-1.0, -0.5, 0.0, 0.5, 1.0])*np.pi)
            grid2[i].xaxis.set_ticklabels(x_label)
            grid2[i].axhline(y = cons.MEDIAN_SPEED*cons.DELTA_V,
                             linewidth=0.5, color='k', linestyle='dashed')
            grid2[i].set_xlabel(r'$\theta_i$ (rad)')
            grid2[i].set_ylabel(r'$v_i$ (BL/s)')


    setter = VelocitySetter(models)
    setter.change_focal_speed_to(cons.MEDIAN_SPEED)
    setter.coordinate_and_plot()

    xlabels = ["{}".format(x) for x in x_a]
    ylabels = ["{}".format(y) for y in y_a]
    xlabels[0] = r'$x_i$=' + xlabels[0]
    xlabels[-1] += ' (BL)'
    ylabels[-1] = r'$y_i$=' + ylabels[-1]
    ylabels[0] += ' (BL)'
    add_axes_labels(fig=fig, axes=ax, xlabels=xlabels, ylabels=ylabels)

    plt.show()


def main():
    parser = generate_arg_parser()
    parser.add_argument('--attention', action='store_true', default=False)
    args = parser.parse_args()
    ### NOT FIXED SPEEDS MUST BE TRUE FOR THE SCRIPT TO WORK
    args.not_fixed_speeds = True
    study_model(args)


if __name__ == '__main__':
    main()
