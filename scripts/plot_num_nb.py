import argparse

import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

from fishandra.plot.num_neighbours import plot_num_nb_histogram, calculate_nb

def plot_num_nb_histogram_from_folder(folder):
    properties = load_properties(folder)
    num_nb = calculate_nb(properties, measure=args.measure)

    fig, ax_hist = plt.subplots(1)
    plot_num_nb_histogram(ax_hist, num_nb)

    if not args.not_save_results:
        print("Creating ", folder / 'num_nb.pdf')
        plt.savefig(folder / 'num_nb.pdf', format='pdf')
    else:
        plt.show()

def load_properties(folder):
    data_file = folder / 'output_results.npy'
    print("Loading ", data_file)
    properties = np.load(data_file, allow_pickle=True).item()
    return properties


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('folder', type=str,
                        help='folder to find output.npy from output_results.py')
    parser.add_argument('--not_save_results', action='store_true')
    parser.add_argument('--measure', type=str, default='inverse_typical_prob',
        help='Measure of num of nb. num_above_typical_prob and inverse_typical_prob')
    args = parser.parse_args()
    plot_num_nb_histogram_from_folder(Path(args.folder))
