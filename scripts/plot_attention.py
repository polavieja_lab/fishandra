import matplotlib.pyplot as plt
import os

from fishandra.plot.new_constants import constants as cons
from fishandra.plot.attention import (attention_nb_focal_speed_figure_data,
                                      plot_fig)
from fishandra.script_utils import (load_model_and_update_constants,
                                    generate_arg_parser)

def plot_attention(args):
    model = load_model_and_update_constants(args)
    Zall, X, Y = attention_nb_focal_speed_figure_data(model)


    plt.ion()
    fig = plt.figure(1, (7, 5))
    plot_fig(fig, X*cons.DELTA_X, Y*cons.DELTA_X, Zall, limits=args.cbar_limits)

    if not args.not_save_results:
        save_path = os.path.join(args.model_folder, 'attention_')
        for n in plt.get_fignums():
            plt.figure(n).savefig(save_path + str(n) + ".pdf", format='pdf')
    else:
        plt.ioff()
        plt.show()


if __name__ == '__main__':
    parser = generate_arg_parser()
    parser.add_argument(
        '--cbar_limits',
        '-l',
        type=int,
        nargs='+',
        default=None)
    args = parser.parse_args()
    plot_attention(args)
