#import os
from pathlib import Path
import argparse
from fishandra.plot.collective_behavior import plot_collective_behaviour

# working

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_folder', type=str)
    parser.add_argument('-ru', '--reuse', action='store_true')
    parser.add_argument('-r', '--remove_outer', type=float, default=.8)
    args = parser.parse_args()
    # loader_cons.USE_PRECALCULATED = False

    plot_collective_behaviour(args)



if __name__ == '__main__':
    main()
