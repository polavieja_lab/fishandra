from copy import deepcopy
import unittest

import fishandra.loader_constants as loader_cons
from fishandra.plot.utils import UpdatingLoaderConstants

# Run as  nosetests -v --nologcapture tests

class TestConstants(unittest.TestCase):
    def test_test(self):
        self.assertTrue(True)
    def test_loader_constants(self):
        keys = loader_cons.variables
        old_cons_dict = {k: deepcopy(loader_cons.__dict__[k]) for k in keys}
        new_cons_dict = {k: None for k in keys}
        print('old_cons_dict ', old_cons_dict)
        print('new_cons_dict ', new_cons_dict)
        with UpdatingLoaderConstants(new_cons_dict):
            changed_cons_dict = {k: deepcopy(loader_cons.__dict__[k]) for k in keys}
            print('changed_cons_dict ', changed_cons_dict)
            self.assertDictEqual(changed_cons_dict, new_cons_dict)
            for key, value in changed_cons_dict.items():
                assert value is None
        after_change_cons_dict = {k: deepcopy(loader_cons.__dict__[k]) for k in keys}
        print('after_change_cons_dict', after_change_cons_dict)
        self.assertDictEqual(old_cons_dict, after_change_cons_dict)
        for key, value in after_change_cons_dict.items():
            assert value is not None


if __name__ == '__main__':
    unittest.main()
