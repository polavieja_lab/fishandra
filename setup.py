from setuptools import setup

setup(name='fishandra',
      version='0.1.1',
      description='fishandra',
      url='http://gitlab.com/polavieja_lab/fishandra',
      author='Francisco J.H. Heras',
      author_email='fjhheras@gmail.com',
      license='GPL',
      packages=['fishandra'],
      scripts=['bin/fastrain', 'bin/fastest'], #'bin/fa-itrain',
      #         'bin/fa-summary', 'bin/fa-benchmark'],
      #package_data={'fishandra': ['data/*.npy']},
      #include_package_data=True,
      zip_safe=False)
