import numpy as np

import logging
logger = logging.getLogger(__name__)


SPEED_CENTILES_TO_PLOT = [5, 35, 65, 95]

def create_constants():
    return Constants()

def mean_accross_configs(data_config, key):
    return np.mean(np.asarray([config[key] for config in data_config]), axis=0)

def default_unit_variables():
    logger.info("default units")
    return {'BL_PX': 80.0, 'R_PX': 1638.0, 'FRAMES_PER_SECOND': 32}

def new_unit_variables_from_file(data_config):
    logger.info("units from file")
    return {'BL_PX' : mean_accross_configs(data_config, 'body_length'),
            'R_PX' : mean_accross_configs(data_config, 'normalization_parameters')[-1],
            'FRAMES_PER_SECOND': mean_accross_configs(data_config, 'frames_per_second')}

def default_speed_variables(delta_v):
    logger.info("default speeds")
    MAX_SPEED_BL = 9  #BL/s
    AVERAGE_SPEED_BL = 3.04 #BL/s
    SPEED_BL = np.array([1, 2, 4, 8]) #BL/s

    variables = {'MAX_SPEED': MAX_SPEED_BL / delta_v,
                 'MEAN_SPEED': AVERAGE_SPEED_BL / delta_v,
                 'SPEED_ALL': SPEED_BL / delta_v}
    variables['MEDIAN_SPEED'] = variables['MEAN_SPEED']
    variables['SPEED'] = variables['SPEED_ALL']
    return variables

def new_speed_variables_from_file(data_config):
    logger.info("speed from file")
    subs = {key: key.lower() for key in ['MAX_SPEED', 'MEDIAN_SPEED',
                                         'MEAN_SPEED', 'MAX_SPEED']}
    subs['SPEED_ALL'] = 'speed_percentiles' #Name to be changed
    variables = {key: mean_accross_configs(data_config, subs[key])
                 for key in subs}
    variables['SPEED'] = [variables['SPEED_ALL'][i]
                          for i in SPEED_CENTILES_TO_PLOT]
    return variables

class Constants():

    @property
    def MAX_X(self):
        return self.MAX_X_BL / self.DELTA_X

    @property
    def BACK_Y(self):
        return self.BACK_Y_BL / self.DELTA_X

    @property
    def SPEED_LABEL_BL(self):
        return ['{:.2f}'.format(s*self.DELTA_V) for s in self.SPEED]

    @property
    def DELTA_X(self):
        return self.R_PX/self.BL_PX

    @property
    def DELTA_V(self):
        return self.DELTA_X * self.FRAMES_PER_SECOND

    @property
    def DELTA_A(self):
        return self.DELTA_X * self.FRAMES_PER_SECOND**2

    @property
    def AVERAGE_SPEED(self):
        logger.warning("AVERAGE_SPEED TO BE DISCONTINUED")
        return self.MEDIAN_SPEED


    # Default plotting
    MAX_X_BL = 8
    BACK_Y_BL = -8
    NUM_PX = 101
    NUM_ANGLES_AVERAGE = 13


    def update_constants(self, *args, **kwargs):
        logger.info('Updating constants...')
        new_constants = self.new_constants_from_config_file(*args, **kwargs)
        self.__dict__.update(new_constants)

    def new_constants_from_config_file(self, data_config, fixed_speeds=False,
                                       fixed_units=False):

        if isinstance(data_config, str): #data_config can be a filename
            data_config = np.load(data_config, allow_pickle=True)

        if fixed_units:
            unit_variables = default_unit_variables()
        else:
            unit_variables = new_unit_variables_from_file(data_config)

        # This sounds like a weird thing to do:
        # In general we should be setting delta_x independently in unit
        # variables. We do it like this because we assume that all data
        # comes normalised by the radius of the arena.
        delta_x = unit_variables['R_PX'] / unit_variables['BL_PX']
        delta_v = delta_x * unit_variables['FRAMES_PER_SECOND']

        if fixed_speeds:
            speed_variables = default_speed_variables(delta_v)
        else:
            speed_variables = new_speed_variables_from_file(data_config)

        acc_variables = {key: mean_accross_configs(data_config, key.lower())
                    for key in ['MAX_ACCELERATION', 'MEDIAN_ACCELERATION']}

        return {**unit_variables, **speed_variables, **acc_variables}

constants = create_constants()
