from copy import copy
import os
import numpy as np
import logging

from .new_constants import constants as cons
from .. import loader_constants as loader_cons
from ..model import load_model_from_file
from ..loader import load_many_datasets_from_file


logger = logging.getLogger(__name__)

class UpdatingLoaderConstants:
    def __init__(self, new_constants):
        self.new_constants = new_constants

    def __enter__(self):
        self.old_constants = {k: copy(loader_cons.__dict__[k])
                              for k in self.new_constants}
        loader_cons.__dict__.update(self.new_constants)

    def __exit__(self, type, value, tb):
        loader_cons.__dict__.update(self.old_constants)



def get_multi_histogram(data, range=None, bins=None):
    if range is None:
        range = [[np.percentile(data[:, 0], 5), np.percentile(data[:, 0], 95)],
                 [np.percentile(data[:, 1], 5), np.percentile(data[:, 1], 95)],
                 [np.percentile(data[:, 2], 5), np.percentile(data[:, 2], 95)],
                 [np.percentile(data[:, 3], 5), np.percentile(data[:, 3], 95)]]
    if bins is None:
        bins = [5, 25, 25, 5]
    h, e = np.histogramdd(data, bins = bins, range = range)
    return h, e, range, bins


def load_model_and_data(model_folder, datafile_index=None,
                        loader_constants_dict={}, new_constants_dict={}):
    """Loads the model and the data that was used to train the model.

    Parameters
    ----------
    model_folder : str
        path to the folder of the model to be loaded.
    datafile_index: int
        if not None, only load dataset with that index
    loader_constants_dict : dict
        dictionary with the variables in constants that should be modified
        ['FRACTIONS', 'LABELS', 'REMOVE_OUTER', 'SHUFFLE', 'PERCENTILES']. It
        is important that the keys of the dictionaries match one of these names.
    new_constants_dict : dict
        dictionary with the values for the boolean variables 'fixed_speeds' and
        'fixed_units' that will passed to the updated_constants method of the
        Constants class in fishandra.plot.new_constnats.

        This is important to make sure that the conversion units (R_BL, ...) are
        correct. For example, when we load the data to be ploted by the collective_behavior
        module

    Returns
    -------
    model :
    datasets :
    """
    data_config_path = os.path.join(model_folder, 'dataset_config.npy')

    # This is necessary so that when we use collective_behavior or data_density
    #we can user the proper units of BL. Maybe consider doing separated on the scripts.
    # But the data config needs to be loaded...
    cons.update_constants(data_config_path,
                          fixed_speeds=new_constants_dict.get('fixed_speeds', False),
                          fixed_units=new_constants_dict.get('fixed_units', False))
    data_config = np.load(data_config_path, allow_pickle=True)
    data_files = [d['data_file'] for d in data_config]

    try:
        from depolaviejadatabase import check_and_update_trajectory_file
        data_files = [check_and_update_trajectory_file(data_file) for data_file in data_files]
    except ImportError:
        logger.info("depolaviejadatabase not installed, trajectories paths won't be checked or updated")

    model = load_model_from_file(model_folder)
    loader_dict = {'num_neighbours': model.args["num_neighbours"],
                   'future_steps': model.args["future_steps"],
                   'blind': model.args["blind"]}

    logger.info("Loading datasets")

    new_constants = {k: loader_constants_dict[k] for k in loader_cons.variables
                     if k in loader_constants_dict}
    if 'REMOVE_OUTER' not in new_constants:
        new_constants['REMOVE_OUTER'] = data_config[0]['remove_outer']
    if datafile_index is not None:
        data_files = [data_files[datafile_index]]
    with UpdatingLoaderConstants(new_constants):
        datasets = load_many_datasets_from_file(data_files, loader_dict)

    return model, datasets


def _social_and_focal_numpy(data, num_neighbours):
    focal, social = np.split(data, [1], axis=1)
    # Here we are removing the position of the focals as it is 0
    # We tile it becuase for every neigbhour we also need the focal variables
    focal_tiled = np.tile(focal[..., 2:], [1, num_neighbours, 1])
    return np.concatenate([social, focal_tiled], axis=2)


def restrict_variables_for_attention_numpy(social_and_focal):
    social_and_focal_sq = np.square(social_and_focal)
    # distance sq, vel sq, acc sq, etc
    attention_variables1 = np.sqrt(
        social_and_focal_sq[..., ::2] + social_and_focal_sq[..., 1::2])
    # speed, focal speed, position
    focal_variables = [attention_variables1[..., 3]] #  focal speed
    neigh_variables = [social_and_focal[..., 0],  # x
                       social_and_focal[..., 1]]  # y
    neigh_variables += [attention_variables1[..., 1]] #  speed
    attention_variables = np.stack(focal_variables + neigh_variables, -1)
    return attention_variables
