from pathlib import Path

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import seaborn as sns
import logging

import trajectorytools as tt

from .. import loader_constants as loader_cons
from .utils import load_model_and_data, _social_and_focal_numpy, \
                restrict_variables_for_attention_numpy, UpdatingLoaderConstants
from .new_constants import constants as cons

logger = logging.getLogger(__name__)
N_BINS_1d = 100
N_BINS_2d = 100

def plot_1d_hist(ax, plot_dict):
    h, e = plot_dict['histogram']
    ax.plot(e[1:]-np.diff(e)[0], h, color='b')
    ax.set_ylabel('pdf')
    ax.set_xlabel(plot_dict['xlabel'])
    ax.set_xlim(plot_dict['h_range'])


    ax.axvline(plot_dict['remove_outer']*cons.DELTA_X, color='r', ls='--')

    sns.despine(ax=ax, top=True, right=True, left=False, bottom=False, offset=None, trim=False)


def plot_1d_dist_shuffled(ax, plot_dict):
    cmap = matplotlib.cm.get_cmap('jet')
    for i, n in enumerate(plot_dict['num_neighbours_list']):
        histograms = plot_dict['all_histograms'][i]
        # color=cmap(n/plot_dict['num_neighbours'])
        color=cmap(n/25)
        h, e = histograms[0]
        ax.plot(e[1:]-np.diff(e)[0], h, color=color, label = '{}'.format(n))
        h, e = histograms[1]
        ax.plot(e[1:]-np.diff(e)[0], h, color=color, ls='--')

    ax.set_xlabel(plot_dict['xlabel'])
    ax.set_ylabel('pdf')
    ax.set_xlim(plot_dict['h_range'])
    leg_nb = ax.legend(title='# neighbours')

    lines = [Line2D([0], [0], color='k', linewidth=1, linestyle=ls) for ls in ['-', '--']]
    labels = ['original', 'shuffled']
    leg_shuffled = ax.legend(lines, labels, title='data type', loc=4)
    ax.add_artist(leg_nb)
    sns.despine(ax=ax, top=True, right=True, left=False, bottom=False, offset=None, trim=False)


def plot_2d_hist(ax, plot_dict):
    fig = ax.get_figure()
    difference = plot_dict['difference']
    extreme = plot_dict['extreme']
    h_range = plot_dict['h_range']
    im = ax.imshow(difference.T, origin='lower',
                   extent=(h_range[0][0], h_range[0][1], h_range[1][0], h_range[1][1]),
                   vmin=-extreme, vmax=extreme, cmap=plt.get_cmap('RdGy'))

    bbox_ax = ax.get_position()
    cax = fig.add_axes([0.87, bbox_ax.y0,
                        0.01, bbox_ax.y1 - bbox_ax.y0])
    cbar = fig.colorbar(im, cax=cax)
    cbar.set_label(r'$pdf$-$pdf_{shuffled}$', rotation=270)
    ax.set_ylabel('Y (BL)')
    ax.set_xlabel('X (BL)')
    ax.set_title('Neigbour positions')


def get_distance_to_center_of_arena_dist(dataset, remove_outer):
    distance_to_the_center = tt.norm(dataset.data['uncollapsed'][:,:2])
    h_range = (0, cons.DELTA_X)
    h, e = np.histogram(distance_to_the_center*cons.DELTA_X,
                        bins=N_BINS_1d,
                        density=True,
                        range=h_range)
    plot_dict = {'histogram': (h, e),
                 'h_range': h_range,
                 'remove_outer': remove_outer,
                 'xlabel': 'Distance to center of the arena (BL)'}
    return plot_dict


def get_inter_individual_distance(dataset, dataset_shuffled):
    distances_matrix = tt.norm(dataset.data['social'][..., :2])
    distances_matrix_shuffled = tt.norm(dataset_shuffled.data['social'][..., :2])
    num_neighbours = dataset.data['social'].shape[1]-1
    num_neighbours_list = [1] + list(range(5, num_neighbours+1, 5))
    max = np.max([np.percentile(distances_matrix, 99.95),
                  np.percentile(distances_matrix_shuffled, 99.95)])
    h_range = (0, max*cons.DELTA_X)
    all_hist = []
    for i in num_neighbours_list:
        h, e = np.histogram(distances_matrix[:, i]*cons.DELTA_X, bins=N_BINS_1d, density=True, range=h_range)
        h_s, e_s = np.histogram(distances_matrix_shuffled[:, i]*cons.DELTA_X, bins=N_BINS_1d, density=True, range=h_range)
        all_hist.append(((h, e), (h_s, e_s)))
    plot_dict = {'all_histograms': all_hist,
                 'num_neighbours_list': num_neighbours_list,
                 'num_neighbours': num_neighbours,
                 'h_range': h_range,
                 'xlabel': 'Inter-individual distance (BL)'}
    return plot_dict


def get_local_polarization_dist(dataset, dataset_shuffled):
    """
    Group average of the module of the polarization vector for n_neigh nearest
    neighbours
    """
    num_neighbours = dataset.data['social'].shape[1]-1
    all_hist = []
    h_range=(0, 1)
    num_neighbours_list = list(range(5, num_neighbours+1, 5))
    for i in num_neighbours_list:
        local_polarization = tt.norm(tt.collective.polarization(dataset.data['social'][:, :i+1, 2:4]))
        local_polarization_shuffled = tt.norm(tt.collective.polarization(dataset_shuffled.data['social'][:, :i+1, 2:4]))
        h, e = np.histogram(local_polarization, bins=N_BINS_1d, density=True, range=h_range)
        h_s, e_s = np.histogram(local_polarization_shuffled, bins=N_BINS_1d, density=True, range=h_range)
        all_hist.append(((h, e), (h_s, e_s)))
    plot_dict = {'all_histograms': all_hist,
                 'num_neighbours_list': num_neighbours_list,
                 'num_neighbours': num_neighbours,
                 'h_range': h_range,
                 'xlabel': 'Local polarization'}
    return plot_dict


def get_position_neighbour_dist(dataset, dataset_shuffled):
    def get_data_neighbours_positions(dataset):
        social = dataset.data['social']
        num_neighbours = dataset.data['social'].shape[1]-1
        social_and_focal = _social_and_focal_numpy(social, num_neighbours)
        data = restrict_variables_for_attention_numpy(social_and_focal) # v, xi, yi, vi
        data = np.reshape(data, [-1, 4])
        data = data[:, 1:3] # get only positions
        return data

    data = get_data_neighbours_positions(dataset) * cons.DELTA_X
    data_shuffled = get_data_neighbours_positions(dataset_shuffled) * cons.DELTA_X
    h_range = [[-2*cons.MAX_X*cons.DELTA_X, 2*cons.MAX_X*cons.DELTA_X],
             [-2*cons.MAX_X*cons.DELTA_X, 2*cons.MAX_X*cons.DELTA_X]]
    bins = N_BINS_2d
    h, e = np.histogramdd(data, bins = bins, range = h_range, normed=True)
    h_s, e = np.histogramdd(data_shuffled, bins = bins, range = h_range, normed=True)

    difference = h-h_s
    extreme = np.max(np.abs(difference))

    plot_dict = {'difference': difference,
                 'extreme': extreme,
                 'h_range': h_range}

    return plot_dict


def get_plot_data(dataset, dataset_shuffled, dataset_with_border, remove_outer):
    # Distance to center of arena
    plot_dict1 = get_distance_to_center_of_arena_dist(dataset_with_border, remove_outer)
    del dataset_with_border
    # Polarization
    plot_dict2 = get_local_polarization_dist(dataset, dataset_shuffled)
    # Interindividual distance and distribution of positions.
    plot_dict3 = get_inter_individual_distance(dataset, dataset_shuffled)
    plot_dict4 = get_position_neighbour_dist(dataset, dataset_shuffled)

    return [plot_dict1, plot_dict2, plot_dict3, plot_dict4]


def plot_data(plot_dict_list):
    fig, ax_arr = plt.subplots(2, 2, figsize=(7, 6))
    plt.subplots_adjust(left=.15, right=0.85, wspace=.3, hspace=.5, top=.9)
    plot_1d_hist(ax_arr[0, 0], plot_dict_list[0])
    plot_1d_dist_shuffled(ax_arr[0, 1], plot_dict_list[1])
    plot_1d_dist_shuffled(ax_arr[1, 0], plot_dict_list[2])
    plot_2d_hist(ax_arr[1, 1], plot_dict_list[3])
    return fig


def save_figure_and_histograms(figure, save_folder, remove_outer):
    fig_name = "collective_behaviour"
    if remove_outer is not None:
        fig_name = fig_name + "_remove_outer_{}.pdf".format(remove_outer)
    figure.savefig(save_folder / fig_name, format='pdf')


def plot_collective_behaviour(args):
    model_folder = Path(args.model_folder).resolve()
    print("Processing... ", model_folder.parent.name)
    if not args.reuse:
        remove_outer_list = [loader_cons.REMOVE_OUTER if args.remove_outer is None else args.remove_outer,
                             loader_cons.REMOVE_OUTER,
                             1.0]
        loader_constants_to_change_list = [{}, {'SHUFFLE': 'trajectories'}, {}]
        messages_list = ['with remove_outer condition',
                         'with remove_outer condition and shuffling trajectories',
                         'without remove_outer condition']
        dataset_list = []
        for i in range(3):
            with UpdatingLoaderConstants(loader_constants_to_change_list[i]):
                logger.info("Loading data files {}".format(messages_list[i]))
                _, datasets = load_model_and_data(model_folder,
                    loader_constants_dict={'REMOVE_OUTER': remove_outer_list[i],
                                           'FRACTIONS': (),
                                           'LABELS': ['train']},
                    new_constants_dict={'fixed_units': True,
                                        'fixed_speeds': True})
                dataset_list.append(datasets.train)

        #remove_outer = loader_cons.REMOVE_OUTER
        remove_outer = remove_outer_list[0]
        plot_dict_list = get_plot_data(*dataset_list,
                                        remove_outer)
        np.save('plot_dict_list.npy', plot_dict_list)

    else:
        data_config_path = model_folder / 'dataset_config.npy'
        cons.update_constants(data_config_path,
                              fixed_speeds=True,
                              fixed_units=True)
        plot_dict_list = np.load('plot_dict_list.npy', allow_pickle=True)
        remove_outer = plot_dict_list[0]['remove_outer']

    fig = plot_data(plot_dict_list)
    fig.suptitle(model_folder.parent.name)
    save_figure_and_histograms(fig, model_folder, remove_outer)
    plt.show()
