import logging
import numpy as np
from mpl_toolkits.axes_grid1 import ImageGrid
from tempfile import TemporaryFile

from .new_constants import constants as cons
from .. import loader_constants
from ..datasetfrontend import BlindingFrontEnd

logger = logging.getLogger(__name__)

def prepare_batch(sn, vf, vn, afn, aft, an, num_neighbours=1):
    assert sn.shape[0] == vf.shape[0] == vn.shape[0] == afn.shape[0] == aft.shape[0] == an.shape[0]

    num_batches = vf.shape[0]
    batch = np.zeros((num_batches, num_neighbours + 1, 6), dtype=np.float32)
    # focal velocity in X, and position (X and Y) are always zero
    batch[:, 0, 3] = vf  # focal velocity
    batch[:, 0, 4] = afn  # focal normal acceleration
    batch[:, 0, 5] = aft  # focal tg acceleration

    # All neighbours have the same properties
    batch[:, 1:, :2] = sn[:, np.newaxis, :]  # neighbours positions
    batch[:, 1:, 2:4] = vn[:, np.newaxis, :]  # neighbours velocities (x, y)
    batch[:, 1:, 4:6] = an[:, np.newaxis, :]  # neighbours acceleration (x, y)
    return batch

def produce_subnetwork_output_many_speeds(model, submodel, Vx, Vy,
                                          x=0, y=0, vf=None, afn=0, aft=0,
                                          an=0, angle_vn=0, angle_an=0):
    '''
    Produces a batch using the input an queries the submodel
    '''

    # Proper defaults
    if vf is None:
        vf = cons.MEDIAN_SPEED

    # Position neighbour
    assert(Vx.shape == Vy.shape)
    vx = Vx.flatten()
    vy = Vy.flatten()
    vn = np.stack([vx, vy], axis=-1)

    # Focal velocity
    vf = np.ones_like(vx)*vf

    # Focal acc
    afn = np.ones_like(vx) * afn
    aft = np.ones_like(vx) * aft
    # Neigbhour acc
    an = [an * np.sin(angle_an), an * np.cos(angle_an)]
    an = np.stack([np.ones_like(vx) * an[0], np.ones_like(vx) * an[1]], axis=-1)

    # Position
    x = np.ones_like(vx) * x
    y = np.ones_like(vx) * y
    sn = np.stack([x, y], axis=-1)

    # Neigbhour velocity
    #vn = [vn * np.sin(angle_vn), vn * np.cos(angle_vn)]
    #vn = np.stack([np.ones_like(x) * vn[0], np.ones_like(x) * vn[1]], axis=-1)
    batch = prepare_batch(sn, vf, vn, afn, aft, an,
                          num_neighbours=model.num_neighbours)
    blinding = BlindingFrontEnd(model.args['blind'])
    batch = blinding({'social': batch})['social']
    Z = getattr(model, submodel).predict(batch,
            batch_size=loader_constants.MAX_BATCH_SIZE)[..., 0]
    #Any last index would do
    return Z.reshape(Vx.shape)


def produce_subnetwork_output(model, submodel, X, Y, vf=None, vn=None,
                              afn=0, aft=0, an=0, angle_vn=0, angle_an=0):
    '''
    Produces a batch using the input an queries the submodel
    '''

    # Proper defaults
    if vf is None:
        vf = cons.MEDIAN_SPEED
    if vn is None:
        vn = cons.MEDIAN_SPEED

    logger.debug("Computing maps: vf {}, vn {}, afn {}, an {}".format(vf, vn, afn, an))

    # Position neighbour
    assert(X.shape == Y.shape)
    x = X.flatten()
    y = Y.flatten()
    sn = np.stack([x, y], axis=-1)

    # Focal velocity
    vf = np.ones_like(x)*vf
    # Neigbhour velocity
    vn = [vn * np.sin(angle_vn), vn * np.cos(angle_vn)]
    vn = np.stack([np.ones_like(x) * vn[0], np.ones_like(x) * vn[1]], axis=-1)
    # Focal acc
    afn = np.ones_like(x) * afn
    aft = np.ones_like(x) * aft
    # Neigbhour acc
    an = [an * np.sin(angle_an), an * np.cos(angle_an)]
    an = np.stack([np.ones_like(x) * an[0], np.ones_like(x) * an[1]], axis=-1)

    batch = prepare_batch(sn, vf, vn, afn, aft, an,
                          num_neighbours=model.num_neighbours)
    blinding = BlindingFrontEnd(model.args['blind'])
    batch = blinding({'social': batch})['social']
    Z = getattr(model, submodel).predict(batch)[..., 0] #Any last index would do
    return Z.reshape(X.shape)

def produce_mesh():
    x = np.linspace(-cons.MAX_X, cons.MAX_X, cons.NUM_PX)
    y = np.linspace(cons.BACK_Y, cons.MAX_X, cons.NUM_PX)
    X, Y = np.meshgrid(x, y)
    return X, Y

def remove_strange_white_lines(CS):
    # To avoid strange white lines
    for c in CS.collections:
        c.set_edgecolor("none")

def produce_grid_figures(fig, ncols=None):
    if ncols is None:
        ncols = len(cons.SPEED)
    grida = ImageGrid(fig, [0.1, 0.5, 0.7, 0.38], axes_pad=(0.1, 0.1),
                      nrows_ncols=(1, ncols))
    gridb = ImageGrid(fig, [0.1, 0.12, 0.7, 0.38], axes_pad=(0.1, 0.1),
                      nrows_ncols=(1, ncols))
    return grida.axes_all + gridb.axes_all

def give_me_cbars(ax_flat, num=1):
    fig = ax_flat[0].get_figure()
    fig.savefig(TemporaryFile()) #Hack to allow getposition
    bbox_axup = ax_flat[0].get_position()
    bbox_axdown = ax_flat[-1].get_position()
    if num == 1:
        cax = fig.add_axes(
            [0.82, bbox_axdown.y0, 0.01, bbox_axup.y1-bbox_axdown.y0])
        return cax
    if num == 2:
        cax_1 = fig.add_axes(
            [0.82, bbox_axdown.y0, 0.01, bbox_axup.y1-bbox_axdown.y0])
        cax_2 = fig.add_axes(
            [0.91, bbox_axdown.y0, 0.01, bbox_axup.y1-bbox_axdown.y0])
        return cax_1, cax_2



